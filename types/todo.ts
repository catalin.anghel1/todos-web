export interface Todo {
    id: number;
    title: string;
    completed: boolean;
}

export interface User {
    userId: number;
    name: string;
    email: string;
    token: string;
}