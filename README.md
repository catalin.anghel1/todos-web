

## Things I could have done better

- Implement a session management system and keeping the user logged in 
- more design tweaks, implementing a theme with a color palette, and so on
- better error handling, ErrorBoundary would have been nice 

But at least I had fun :-) 
