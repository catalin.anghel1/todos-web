import axios from 'axios';
import { Todo } from '../../types/todo';

const API_BASE_URL = 'http://localhost:8080';

export const api = axios.create({
  baseURL: API_BASE_URL,
});

export const signUp = (name: string, email: string, password: string) => {
  return api.post('/signup', { name, email, password });
};

export const login = (email: string, password: string) => {
  return api.post('/login', { email, password });
};

export const getTodos = (token: string) => {
  return api.get<{ todos: Todo[]}>('/todos', {
    headers: {
      'Authorization': `Basic ${token}` 
    }
  });
};

export const addTodo = (title: string, token: string) => {
  return api.post<Todo>('/todos', { title, completed: false }, {
    headers: {
      'Authorization': `Basic ${token}` 
    }
  });
};

export const updateTodo = (id: number, config: Partial<Pick<Todo, 'title' | 'completed'>>, token: string) => {
  const state = config.completed ? 'completed' : 'uncompleted';
  return api.put<Todo>(`/todos/${id}/${state}`, config, {
    headers: {
      'Authorization': `Basic ${token}` 
    }
  });
};

export const deleteTodo = (id: number, token: string) => {
  return api.delete(`/todos/${id}`, {
    headers: {
      'Authorization': `Basic ${token}` 
    }
  });
};
