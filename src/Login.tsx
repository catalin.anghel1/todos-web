import {useState, useContext} from 'react';
import {useNavigate, Link as RouterLink} from 'react-router-dom';
import {Avatar, Button, TextField, Link, Typography, Container} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {AuthContext} from './context/AuthContext';
import { Alert } from '@material-ui/lab';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundImage: 'url(logo.svg)',
        backgroundPosition: 'center',
        backgroundColor: 'transparent',
        backgroundRepeat: 'no-repeat',
        alignSelf: 'flex-start',
        borderRadius: 0,
    },
    title: {
        color: '#1f2a4b',
    },
    subtitle: {
        color: '#a1a4ad',
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    signup: {
        color: '#1f2a4b',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        textTransform: 'capitalize',
        background: '#4a77e5',
    },
}));

export const Login = () => {
    const classes = useStyles();
    const navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const {authenticate} = useContext(AuthContext);

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            await authenticate(email, password);
            navigate('/todos');
        } catch (error: any) {
            setError(error.message);
        }

    };

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <span></span>
                </Avatar>
                <Typography component="h1" variant="h5" className={classes.title}>
                    Welcome back
                </Typography>
                <Typography component="h2" variant="body2" className={classes.subtitle}>
                    Log in to continue.
                </Typography>
                {error && <Alert severity="error">{error}</Alert>}
                <form className={classes.form} onSubmit={handleSubmit}>
                    <TextField
                        variant="standard"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}
                    />
                    <TextField
                        variant="standard"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                    />
                    <Link to="/signup" variant="body2" className={classes.signup} component={RouterLink}>
                        Don't have an account? Sign Up.
                    </Link>
                    <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
                        Sign in
                    </Button>
                </form>
            </div>
        </Container>
    );
};
