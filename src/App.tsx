import React, {useContext} from 'react';
import {AuthContext} from './context/AuthContext';
import {Login} from './Login';
import {Signup} from './Signup';
import {TodoList} from './TodoList';
import {TodoProvider} from './context/TodoContext';

import {BrowserRouter as Router, Routes, Route, Navigate} from 'react-router-dom';


const App = () => {
    const {user} = useContext(AuthContext);

    const isAuthenticated = Boolean(user);

    return (
        <Router>
            <Routes>
                <Route path="/login" element={isAuthenticated ? <Navigate to="/todos" /> : <Login />} />
                <Route path="/signup" element={isAuthenticated ? <Navigate to="/todos" /> : <Signup />} />
                <Route path="/todos" element={isAuthenticated ? <TodoProvider><TodoList /></TodoProvider> : <Navigate to="/login" />} />
                <Route path="/" element={<Navigate to="/todos" />} />
            </Routes>
        </Router>
    );
};

export default App;
