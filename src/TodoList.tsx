import React, {useContext, useMemo, useState} from 'react';
import {
    Checkbox,
    Grid,
    IconButton,
    Typography,
    Link,
    Avatar,
    TextField,
    Container,
    makeStyles,
} from '@material-ui/core';
import {Close} from '@material-ui/icons';
import {TodoContext} from './context/TodoContext';
import {Todo} from '../types/todo';
import {Alert} from '@material-ui/lab';

enum FILTER {
    ALL = 0,
    INCOMPLETED = 1,
    COMPLETED = 2,
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundImage: 'url(logo.svg)',
        backgroundPosition: 'center',
        backgroundColor: 'transparent',
        backgroundRepeat: 'no-repeat',
        alignSelf: 'flex-start',
        borderRadius: 0,
    },
    title: {
        color: '#1f2a4b',
    },
    todos: {
        marginTop: theme.spacing(2),
    },
    filter: {
        color: '#1f2a4b',
        marginTop: theme.spacing(1),
        cursor: 'pointer',
    },
    filters: {
        marginTop: theme.spacing(3),
    },
}));

export const TodoList = () => {
    const classes = useStyles();
    const [title, setTitle] = useState('');
    const {todos, addTodo, deleteTodo, updateTodo} = useContext(TodoContext);
    const [filter, setFilter] = useState(FILTER.ALL);
    const [error, setError] = useState('');

    const handleCreateTodo = async () => {
        try {
            await addTodo(title);
            setTitle('');
        } catch (error: any) {
            setError(error.message);
        }
    };

    const handleToggleComplete = async (todoId: number, completed: boolean) => {
        await updateTodo(todoId, {completed: !completed});
    };

    const handleDeleteTodo = async (todoId: number) => {
        await deleteTodo(todoId);
    };

    const filteredTodos = useMemo(() => {
        switch (filter) {
            case FILTER.COMPLETED:
                return todos.filter((todo) => todo.completed);
            case FILTER.INCOMPLETED:
                return todos.filter((todo) => !todo.completed);
            case FILTER.ALL:
            default:
                return todos;
        }
    }, [todos, filter]);

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <span></span>
                </Avatar>
                <Typography component="h1" variant="h5" className={classes.title}>
                    Todo List
                </Typography>
                {error && <Alert severity="error">{error}</Alert>}
                <TextField
                    variant="standard"
                    margin="normal"
                    required
                    fullWidth
                    id="title"
                    label="Add a new todo"
                    name="title"
                    autoFocus
                    value={title}
                    onChange={(event) => setTitle(event.target.value)}
                    onKeyDown={(event) => {
                        if (event.key === 'Enter') {
                            handleCreateTodo();
                        }
                    }}
                />

                <Grid container spacing={1} direction="column" className={classes.todos}>
                    {filteredTodos.length ? (
                        filteredTodos.map((todo: Todo) => {
                            return (
                                <Grid container direction="row" key={todo.id} alignItems="center">
                                    <Grid item>
                                        <Checkbox
                                            checked={todo.completed}
                                            onChange={() => handleToggleComplete(todo.id, todo.completed)}
                                            color="primary"
                                        />
                                    </Grid>
                                    <Grid item>
                                        <Typography
                                            variant="body1"
                                            component="span"
                                            style={{textDecoration: todo.completed ? 'line-through' : 'none'}}
                                        >
                                            {todo.title}
                                        </Typography>
                                    </Grid>
                                    <Grid
                                        item
                                        style={{
                                            alignSelf: 'flex-end',
                                        }}
                                    >
                                        <IconButton color="primary" onClick={() => handleDeleteTodo(todo.id)}>
                                            <Close />
                                        </IconButton>
                                    </Grid>
                                </Grid>
                            );
                        })
                    ) : (
                        <Typography component="span" variant="body2" className={classes.title}>
                            {'Hurray! nothing to do today, must be Sunday ;-)'}
                        </Typography>
                    )}
                </Grid>

                <Grid container spacing={3} alignItems="flex-start" direction="row" className={classes.filters}>
                    <Grid item>
                        <Typography component="span" variant="body2" className={classes.title}>
                            Show:
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Link className={classes.filter} onClick={() => setFilter(FILTER.ALL)}>
                            All
                        </Link>
                    </Grid>
                    <Grid item>
                        <Link className={classes.filter} onClick={() => setFilter(FILTER.COMPLETED)}>
                            Completed
                        </Link>
                    </Grid>
                    <Grid item>
                        <Link className={classes.filter} onClick={() => setFilter(FILTER.INCOMPLETED)}>
                            Incompleted
                        </Link>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};
