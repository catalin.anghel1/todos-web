import React, {createContext, PropsWithChildren, useContext, useEffect, useState} from 'react';
import {Todo} from '../../types/todo';
import {addTodo, deleteTodo, getTodos, updateTodo} from '../api/endpoints';
import {AuthContext} from './AuthContext';

interface TodoContextValue {
    todos: Todo[];
    filteredTodos: Todo[];
    addTodo: (title: string) => Promise<void>;
    updateTodo: (id: number, config: Partial<Pick<Todo, 'title' | 'completed'>>) => Promise<void>;
    deleteTodo: (id: number) => Promise<void>;
    filterCompleted: () => void;
    filterIncomplete: () => void;
    clearFilter: () => void;
}

export const TodoContext = createContext<TodoContextValue>({
    todos: [],
    filteredTodos: [],
    addTodo: async () => {},
    updateTodo: async () => {},
    deleteTodo: async () => {},
    filterCompleted: () => {},
    filterIncomplete: () => {},
    clearFilter: () => {},
});

export const TodoProvider = ({children}: PropsWithChildren<{}>) => {
    const [todos, setTodos] = useState<Todo[]>([]);
    const [filteredTodos, setFilteredTodos] = useState<Todo[]>([]);
    const {user} = useContext(AuthContext);

    const token = user ? user.token : '';

    const fetchTodos = async () => {
        const response = await getTodos(token);
        setTodos(response.data?.todos);
        setFilteredTodos(response.data?.todos);
    };

    useEffect(() => {
        fetchTodos();
    }, []);

    const addTodoHandler = async (title: string) => {
        const response = await addTodo(title, token);
        
        if (response.data) {
            fetchTodos();
        }
    };

    const updateTodoHandler = async (id: number, config: Partial<Pick<Todo, 'title' | 'completed'>>) => {
        const response = await updateTodo(id, config, token);

        if (response.data) {
            fetchTodos();
        }
    };

    const deleteTodoHandler = async (id: number) => {
        const response = await deleteTodo(id, token);

        if (response.data) {
            fetchTodos();
        }
    };

    const filterCompletedHandler = () => {
        const completedTodos = todos.filter((todo) => todo.completed);
        setFilteredTodos(completedTodos);
    };

    const filterIncompleteHandler = () => {
        const incompleteTodos = todos.filter((todo) => !todo.completed);
        setFilteredTodos(incompleteTodos);
    };

    const clearFilterHandler = () => {
        setFilteredTodos(todos);
    };

    return (
        <TodoContext.Provider
            value={{
                todos: user ? filteredTodos : [],
                filteredTodos,
                addTodo: addTodoHandler,
                updateTodo: updateTodoHandler,
                deleteTodo: deleteTodoHandler,
                filterCompleted: filterCompletedHandler,
                filterIncomplete: filterIncompleteHandler,
                clearFilter: clearFilterHandler,
            }}
        >
            {children}
        </TodoContext.Provider>
    );
};
