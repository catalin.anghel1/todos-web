import React, {createContext, PropsWithChildren, useState} from 'react';
import {User} from '../../types/todo';
import {signUp, login} from '../api/endpoints';

interface AuthContextValue {
    user: User | null;
    createAccount: (name: string, email: string, password: string) => Promise<void>;
    authenticate: (email: string, password: string) => Promise<void>;
    logout: () => void;
}

export const AuthContext = createContext<AuthContextValue>({
    user: null,
    createAccount: async () => {},
    authenticate: async () => {},
    logout: () => {},
});

export const AuthProvider = ({children}: PropsWithChildren<{}>) => {
    const [user, setUser] = useState<User | null>(null);

    const createAccount = async (name: string, email: string, password: string) => {
        const response = await signUp(name, email, password);

        if (response.status >= 200 && response.status <= 299) {
           console.log('Signed up successfully!');
        } else {
            throw new Error('Unable to sign up!');
        }
    };

    const authenticate = async (email: string, password: string) => {
        const response = await login(email, password);

        if (response.data) {
            setUser({
                ...response.data.user,
                token: response.data.token,
            });

            console.log(response.data.user.email, 'logged in successfully!');
        } else {
            throw new Error('Login failed');
        }
    };

    const logout = () => {
        setUser(null);
    };

    return <AuthContext.Provider value={{user, createAccount, authenticate, logout}}>{children}</AuthContext.Provider>;
};
